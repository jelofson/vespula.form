<?php

namespace Vespula\Form;

use Vespula\Form\Element\Text;
use Vespula\Form\Element\Password;
use Vespula\Form\Element\Textarea;
use Vespula\Form\Element\Select;
use Vespula\Form\Element\Radio;
use Vespula\Form\Element\Checkbox;
use Vespula\Form\Element\Button;
use Vespula\Form\Element\Label;
use Vespula\Form\Element\Hidden;
use Vespula\Form\Builder\BuilderInterface;

/**
 * This class is used as a basic HTML form builder.
 *
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */
class Form
{
    /**
     * An array of elements added to the form via addElement()
     *
     * @var array
     */
    protected $elements = [];

    /**
     * An attribute object used to manage attributes and values
     *
     * @var Vespula\Form\Attributes
     */
    protected $attributes;

    /**
     * The form builder for automatically creating form elements from a db table
     *
     * @var BuilderInterface
     */
    protected $builder;

    /**
     *
     * @param string $method
     */
    public function __construct($method = 'post')
    {

        $this->attributes = new Attributes(); // Bad Dependency here
        $this->attributes->set('method', $method);
    }

    /**
     * This is to allow a method called `class`
     * @param  string $method
     * @param  array $attribs
     * @return Vespula\Form\Form
     */
    public function __call($method, $attribs)
    {
        if ($method !== 'class') {
            throw new \InvalidArgumentException();
        }
        $value = null;

        if (isset($attribs[0])) {
            $value = $attribs[0];
        }

        return $this->setClass($value);
    }

    /**
     * Set the form method, overriding what was set at construction
     *
     * @param string $method
     */
    public function method($method)
    {
        $this->attributes->set('method', $method);
        return $this;
    }

    /**
     * Set the form id attribute
     *
     * @param string $id
     * @return \Vespula\Form\Form
     */
    public function id($id)
    {
        $this->attributes->set('id', $id);
        return $this;
    }

    /**
     * Sets the action attribute
     * @param  string $action The form action
     * @return \Vespula\Form\Form
     */
    public function action($action)
    {
        $this->attributes->set('action', $action);
        return $this;
    }

    /**
     * Sets the form name attribute
     * @param  string $name
     * @return \Vespula\Form\Form
     */
    public function name($name)
    {
        $this->attributes->set('name', $name);
        return $this;
    }

    /**
     * Sets the form class attribute
     * @param  string $class
     * @return \Vespula\Form\Form
     */
    public function setClass($class)
    {
        $this->attributes->set('class', $class);
        return $this;
    }

    /**
     * Sets the enctype to multipart/form-data
     * @return \Vespula\Form\Form
     */
    public function multipart()
    {
        $this->attributes->set('enctype', 'multipart/form-data');
        return $this;
    }

    /**
     * Sets a miscellaneous form attribute.
     * If $val is null, the attribute will return in this format:
     * <form myattribute />
     *
     * @param  string $key The attribute name
     * @param  string $val The attribute value
     * @return \Vespula\Form\Form
     */
    public function attribute($key, $val = null)
    {
        $this->attributes->set($key, $val);
        return $this;
    }

    /**
     * Create the opening form tag using the defined attributes
     * @return string the opening form tag
     */
    public function begin()
    {
        return '<form' . $this->attributes . '>' . PHP_EOL;
    }

    /**
     * Close the form tag
     * @return string The closing form tag
     */
    public function end()
    {
        return '</form>' . PHP_EOL;
    }

    /**
     * Set the element class to auto lf. This makes it so you don't have to
     * call lf() after each element
     *
     * @return $this
     */
    public function autoLf()
    {
        \Vespula\Form\Element\Element::$autoLf = true;
        return $this;
    }

    /**
     * Put auto line feed back to false
     *
     * @return $this
     */
    public function autoLfOff()
    {
        \Vespula\Form\Element\Element::$autoLf = false;
        return $this;
    }

    /**
     * Add a form element to be retrieved later
     * @param string $id The key used to retrieve the element later
     * @param \Vespula\Form\Element\Element $element The element
     */
    public function addElement($id, \Vespula\Form\Element\Element $element)
    {
        $this->elements[$id] = $element;
        return $this;
    }
    
    /**
     * Re-order the elements in the form by specifying their ids in `$element_ids_reordered` in the expected order.
     * Items in `$element_ids_reordered` that do not correspond to an existing element's id in the form will be ignored.
     * 
     * @param array $element_ids_reordered an array containing ids of one or more elements that have been added to this form
     * @return $this
     */
    public function reorderElements(array $element_ids_reordered)
    {
        $existing_elements = $this->elements; // make a copy
        $this->elements = []; // reset elements
        
        foreach ($element_ids_reordered as $id) {
            if (array_key_exists($id, $existing_elements)) {
                $this->elements[$id] = $existing_elements[$id];
                
                // remove from old existing elements array
                unset($existing_elements[$id]);
            }
        }
        
        // add remaining existing elements with original ordering to
        // array of form's elements since their order was not specified
        // via $element_ids_reordered
        foreach ($existing_elements as $id => $existing_element) {
            $this->elements[$id] = $existing_element;
        }
        
        return $this;
    }

    /**
     * Retrieve and element by key
     * @param  string $id The key associated with the element in the element store
     * @return false | \Vespula\Form\Element\Element
     */
    public function getElement($id)
    {
        if (array_key_exists($id, $this->elements)) {
            return $this->elements[$id];
        }

        throw new \Exception("Element with id `$id` was not found in the element store");
    }

    /**
     * Get all the stored elements as an array
     *
     * @return array
     */
    public function getElements(array $keys = null)
    {
        if (! $keys) {
            return $this->elements;
        }
        $elements = [];
        foreach ($keys as $id) {
            $elements[$id] = $this->getElement($id);
        }
        return $elements;

    }
    
    /**
     * Reset the form
     *
     * @return $this
     */
    public function reset()
    {
        $this->attributes->reset();
        $this->elements = [];
        $this->autoLfOff();
        return $this;
    }

    /**
     * Create a new label element
     * @param  string $text The label text
     * @return \Vespula\Form\Element\Label
     */
    public function label($text = null)
    {
        $label = new Label();
        $label->text($text);
        return $label;
    }

    /**
     * Create a new input type text element
     * @return \Vespula\Form\Element\Text
     */
    public function text()
    {
        $textElement = new Text();
        return $textElement;
    }

    /**
     * Create a new input type password element
     * @return \Vespula\Form\Element\Text
     */
    public function password()
    {
        $passwordElement = new Password();
        return $passwordElement;
    }

    /**
     * Create a new textarea element
     * @return \Vespula\Form\Element\Textarea
     */
    public function textarea()
    {
        $textareaElement = new Textarea();
        return $textareaElement;
    }

    /**
     * Create a new hidden input element
     * @return \Vespula\Form\Element\Hidden
     */
    public function hidden()
    {
        $hiddenElement = new Hidden();
        return $hiddenElement;
    }

    /**
     * Create a new select element
     * @return \Vespula\Form\Element\Select
     */
    public function select()
    {
        $select = new Select();
        return $select;
    }

    /**
     * Create a new radio input element
     * @return \Vespula\Form\Element\Radio
     */
    public function radio()
    {
        $radio = new Radio();
        return $radio;
    }

    /**
     * Create a new checkbox input element
     * @return \Vespula\Form\Element\Checkbox
     */
    public function checkbox()
    {
        $radio = new Checkbox();
        $this->output[] = $radio;
        return $radio;
    }

    /**
     * Create a new button element
     * @param string $text The button text
     * @return \Vespula\Form\Element\Button
     */
    public function button($text = null)
    {
        $button = new Button();
        $button->text($text);
        return $button;
    }

    /**
     * Create a new submit button element
     * @param string $text The button text
     * @return \Vespula\Form\Element\Button
     */
    public function submit($text = null)
    {
        $button = new Button();
        $button->text($text);
        $button->type('submit');
        return $button;
    }

    /**
     * Set the builder object
     *
     * @param BuilderInterface $builder
     */
    public function setBuilder(BuilderInterface $builder)
    {
        $this->builder = $builder;
    }

    /**
     * Get the builder
     * @return BuilderInterface The builder
     */
    public function getBuilder()
    {
        return $this->builder;
    }

    public function build($table, $data = null)
    {
        $builder = $this->builder;

        return $builder->build($this, $table, $data);
    }
}
