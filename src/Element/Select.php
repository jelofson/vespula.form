<?php
namespace Vespula\Form\Element;


/**
 * Creates a select form element
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */
class Select extends Element
{
    /**
     * The options for the select
     * @var array
     */
    protected $options = [];

    /**
     * Specifies what the selected value(s) should be
     * @var array
     */
    protected $selected = [];
    
    /**
     * A single dimensional array containing values of the options that should be disabled
     * @var array
     */
    protected $disabled_options = [];
    
    /**
     * A flag indicating whether or not all options should be disabled in the select. 
     * `True` disables all options regardless of the state of $this->disabled_options.
     * `False` leaves options in their configured state.
     * @var bool 
     */
    protected $all_options_disabled = false;
    
    /**
     * A flag indicating whether or not all options should be selected in the select. 
     * `True` selects all options.
     * `False` leaves options in their configured state.
     * @var bool 
     */
    protected $all_options_selected = false;

    /**
     * An indent number for the options and select closing tag
     * @var integer
     */
    protected $indent = 0;

    /**
     * Specify the options for the select. Either a simple array or multidimentional
     * array. multidimentional arrays produce optgroups
     *
     * @param  array  $options
     * @return \Vespula\Form\Element\Select
     */
    public function options(array $options)
    {
        $this->options = $options;
        return $this;
    }
    /**
     * An array of options set to 'disabled'
     * 
     * @param array $disabled_options
     * @return $this
     */
    public function disabledOptions(array $disabled_options) {
        
        $this->disabled_options = $disabled_options;
        return $this;
    }
    
    /**
     * Determines if all options should be disabled
     * 
     * @param bool $disabled
     * @return $this
     */
    public function allOptionsDisabled($disabled) {
        
        $this->all_options_disabled = (bool) $disabled;
        
        return $this;
    }
    
    /**
     * Specify if all options should be selected or not
     * 
     * @param bool $all_selected
     * @return $this
     */
    public function allOptionsSelected($all_selected) {
        
        $this->all_options_selected = (bool) $all_selected;
        
        return $this;
    }

    /**
     * Specify the selected value which will set that option to 'selected'
     * @param  string|array $value
     * @return \Vespula\Form\Element\Select
     */
    public function selected($value)
    {
        $this->selected = (array) $value;
        return $this;
    }

    /**
     * Set the multiple attribute
     * 
     * @return \Vespula\Form\Element\Select
     */
    public function multiple()
    {
        $this->attribute('multiple', true);
        return $this;
    }

    /**
     * Set the indent of the options and the closing select tag
     * @param  integer $num
     * @return \Vespula\Form\Element\Select
     */
    public function indent($num = 0)
    {
        $this->indent = $num;
        return $this;
    }

    /**
     * Output the select element as a string
     * @return string
     */
    public function __toString(): string
    {
        if (self::$autoLf) {
            $this->lf();
        }
        $this->attributes->remove('type');
        $this->attributes->remove('value');

        $indent = str_repeat('    ', $this->indent);

        $start = $this->start();
        $options = $this->buildOptions($indent);
        $end = $indent . $this->end();

        if ($options) {
            return $start . PHP_EOL . $options . PHP_EOL . $end . $this->lf;
        }
        return $start . $end . $this->lf;

    }

    /**
     * Create the starting tag
     * @return string
     */
    protected function start()
    {
        return '<select' . $this->attributes . '>';
    }

    /**
     * Create the ending tag
     * @return string
     */
    protected function end()
    {
        return '</select>';
    }

    /**
     * Build the options from options array
     * @param  integer $indent
     * @return string Option tags
     */
    protected function buildOptions($indent = null)
    {
        $options = [];
        foreach ($this->options as $key=>$val) {
            if (is_array($val)) {
                $options[] = $indent . '    <optgroup label="' . $key . '">';
                foreach ($val as $optkey=>$optval) {
                    $options[] = $indent . '        ' . $this->buildOption($optkey, $optval);
                }
                $options[] = $indent . '    </optgroup>';
                continue;
            }
            $options[] = $indent . '    ' . $this->buildOption($key, $val);

        }

        return implode(PHP_EOL, $options);
    }

    /**
     * Build individual options
     * @param  string $key The option value
     * @param  string $val The option text
     * @return string The option tag
     */
    protected function buildOption($key, $val)
    {
        $key = (string) $key;
        $selected = ( in_array($key, $this->selected) || ((bool) $this->all_options_selected) ) ? ' selected' : '';
        $disabled = ( in_array($key, $this->disabled_options) || ((bool) $this->all_options_disabled) ) ? ' disabled' : '';

        return '<option value="' . $key . '"' . $selected . $disabled . '>' . $val . '</option>';
    }
}
