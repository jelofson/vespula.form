<?php
namespace Vespula\Form\Element;


/**
 * Creates a button form Element
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */
class Button extends Element
{
    /**
     * The button text
     * @var string
     */
    protected $text;

    /**
     * Constructor. Sets the default type to 'button'
     */
    public function __construct()
    {
        parent::__construct();
        $this->attributes->set('type', 'button');
    }

    /**
     * Set the button text
     * @param  string $text
     * @return \Vespula\Form\Element\Button
     */
    public function text($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * Sets the button type. Only button, submit, or reset are allowed
     * @param  string $type
     * @return \Vespula\Form\Element\Button
     */
    public function type($type)
    {
        $valid = [
            'button',
            'submit',
            'reset'
        ];

        if (! in_array($type, $valid)) {
            throw new \InvalidArgumentException('Invalid button type');
        }
        $this->attributes->set('type', $type);
        return $this;
    }
    
    /**
     * Returns the string representation of the button tag
     * @return string
     */
    public function __toString(): string
    {
        if (self::$autoLf) {
            $this->lf();
        }
        return '<button' . $this->attributes . '>' . $this->text . '</button>' . $this->lf;
    }
}
