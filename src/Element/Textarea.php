<?php
namespace Vespula\Form\Element;


/**
 * Create a textarea form element
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */
class Textarea extends Element
{

    /**
     * Sets the rows attribute
     * @param  integer $rows
     * @return \Vespula\Form\Element\Textarea
     */
    public function rows($rows)
    {
        $this->attributes->set('rows', $rows);
        return $this;
    }

    /**
     * Sets the cols attribute
     * @param  integer $cols
     * @return \Vespula\Form\Element\Textarea
     */
    public function cols($cols)
    {
        $this->attributes->set('cols', $cols);
        return $this;
    }

    /**
     * Output the textarea element as a string
     * @return string 
     */
    public function __toString(): string
    {
        if (self::$autoLf) {
            $this->lf();
        }
        $value = $this->attributes->get('value') ? $this->attributes->get('value') : null;
        
        $attribs = clone $this->attributes; 
        $attribs->remove('value'); 
        return '<textarea' . $attribs . '>' . $value . '</textarea>' . $this->lf; 

    }
}
