<?php
namespace Vespula\Form\Element;


/**
 * Creates a label form element
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */
class Label extends Element
{
    const BEFORE = 'before';
    const AFTER = 'after';

    /**
     * Label text
     * @var string
     */
    protected $text;

    /**
     * An indent value for optional wrapped elements and the closing tag
     * @var integer
     */
    protected $indent = 0;

    /**
     * An element that may be wrapped by the label via the wrap() method
     * @var \Vespula\Form\Element\Element
     */
    protected $element;

    protected $wrap_position;

    public function __construct()
    {
        parent::__construct();
        array_push($this->validAttribs, 'for');
    }

    /**
     * Set the label text
     * @param  string $text
     * @return \Vespula\Form\Element\Label
     */
    public function text($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * Wrap the label around this element
     * @param  \Vespula\Form\Element\Element $element
     * @return \Vespula\Form\Element\Label
     */
    public function wrap(Element $element)
    {
        $this->element = $element;
        $this->wrap_position = self::BEFORE;
        return $this;
    }

    /**
     * Wrap the label around this element with the label text AFTER the element
     * @param  \Vespula\Form\Element\Element $element
     * @return \Vespula\Form\Element\Label
     */
    public function wrapAfter(Element $element)
    {
        $this->element = $element;
        $this->wrap_position = self::AFTER;
        return $this;
    }


    /**
     * Set an indent for the optional wrapped element and the closing tag
     * @param  integer $num Number of indents (2 would mean 8 spaces)
     * @return \Vespula\Form\Element\Label
     */
    public function indent($num = 0)
    {
        $this->indent = $num;
        return $this;
    }

    protected function wrapElement($open, $close, $indent)
    {
        if ($this->wrap_position == self::BEFORE) {
            $output =  $open . $this->text . PHP_EOL;
            $output .= $indent . '    ' . $this->element;
            $output .= self::$autoLf ? null : PHP_EOL;
            $output .= $close;

            return $output . $this->lf;
        }

        if ($this->wrap_position == self::AFTER) {
            $output =  $open . PHP_EOL;
            $output .= $indent . '    ' . $this->element;
            $output .= $this->text . PHP_EOL;
            $output .= $close;

            return $output . $this->lf;
        }
    }


    /**
     * Output the label as a string
     * @return string
     */
    public function __toString(): string
    {
        if (self::$autoLf) {
            $this->lf();
        }
        $indent = str_repeat('    ', $this->indent);

        $open = '<label' . $this->attributes . '>';
        $close = $indent . '</label>';

        if ($this->element) {
            return $this->wrapElement($open, $close, $indent);
        }

        return $open . $this->text . $close . $this->lf;

    }

}
