<?php
namespace Vespula\Form\Element;


/**
 * Create a password text input element
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */
class Password extends Input
{
    /**
     * The input type (required)
     * @var string
     */
    protected $type = 'password';

    /**
     * Set the input type (for use with HTML5 text inputs, such as email, color, etc)
     * @param  string $type
     * @return \Vespula\Form\Element\Text
     */
    public function type($type)
    {
        $this->type = $type;
        $this->attribute('type', $type); // also update the attribute
        
        return $this;
    }

    /**
     * Output the element as a string
     * @return string 
     */
    public function __toString(): string
    {
        if (self::$autoLf) {
            $this->lf();
        }
        $this->attributes->set('type', $this->type);
        
        return '<input' . $this->attributes . ' />' . $this->lf;
    }
}
