<?php

namespace Vespula\Form\Builder;


use Vespula\Form\Form;
use Vespula\Form\Element\Input;
use Vespula\Form\Element\Checkbox;
use Vespula\Form\Element\Radio;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Doctrine\DBAL\Schema\Column;

/**
 * A form builder which creates basic form elements based on column definitions
 * in a database table. Optionally sets the element values based on data.
 *
 */
class Builder implements BuilderInterface
{
    /**
     * The initial form object
     * @var Form
     */
    protected $form;

    /**
     * The schema object used to get column definitions from the db
     * @var AbstractSchemaManager
     */
    protected $schemaManager;

    /**
     * Default textarea cols
     *
     * @var integer
     */
    protected $textareaCols = 10;

    /**
     * Default textarea rows
     * @var integer
     */
    protected $textareaRows = 5;

    /**
     * The default column mapping.
     *
     * @var array
     */
    protected $map = [
        'text'=>'textarea',
        'tinytext'=>'textarea',
        'mediumtext'=>'textarea',
        'longtext'=>'textarea',
    ];

    /**
     * Default CSS classes for each element type
     *
     * @var array
     */
    protected $defaultClasses = [];

    /**
     * Column options with keys of `type`, `raw`, and `callback`
     *
     * @var array
     */
    protected $columnOptions = [];

    /**
     * Default options for columns. Ensures each key is set.
     *
     * @var array
     */
    protected $defaultOptions = [
        'type'=>null,
        'raw'=>false,
        'callback'=>null
    ];

    /**
     * Columns to blacklist (don't create elements for)
     *
     * @var array
     */
    protected $blackList = [];

    /**
     * Constructor
     * @param Connection $conn Doctrine DBAL Connection
     */
    public function __construct(Connection $conn)
    {
        
        $this->schemaManager = $conn->createSchemaManager();
    }

    /**
     * Override the initially created SchemaManager object
     * @param AbstractSchemaManager $schema 
     */
    public function setSchema(AbstractSchemaManager $schemaManager)
    {
        $this->schemaManager = $schemaManager;
    }

    /**
     * Build the form looking up column defs in the db and assigned values if a
     * data array was passed.
     *
     * @param  Form   $form  A Form to modify/add elements to
     * @param  string $table A db table
     * @param  array|ArrayAccess $data  Data to set the form element values.
     * @return Form A form with new elements.
     */
    public function build(Form $form, $table, $data = null)
    {
        if ($data && ! $data instanceof \ArrayAccess && ! is_array($data)) {
            $type = ucfirst(gettype($data));
            throw new \InvalidArgumentException('Argument 2 must be an array or implement ArrayAccess. ' . $type . ' given.');
        }

        $cols = $this->schemaManager->listTableColumns($table);

        // Remove blacklisted columns
        $cols = array_diff_key($cols, array_flip($this->blackList));

        // Each column will have default options, normalize custom, if set
        $this->normalizeOptions($cols);

        foreach ($cols as $columnName=>$columnObject) {

            // Determine the form element type to create
            $type = $this->getColumnType($columnName, $columnObject);

            switch ($type) {
                case 'textarea' :
                    $element = $form->textarea()->cols($this->textareaCols)->rows($this->textareaRows);
                    break;
                case 'password' :
                    $element = $form->password();
                    break;
                case 'checkbox' :
                    $element = $form->checkbox();
                    break;
                case 'radio' :
                    $element = $form->radio();
                    break;
                case 'select' :
                    $element = $form->select();
                    break;
                case 'button' :
                    $element = $form->button(ucfirst($columnName));
                    break;
                case 'submit' :
                    $element = $form->submit(ucfirst($columnName));
                    break;
                case 'text' :
                    $element = $form->text();
                    break;
                default :
                    $element = $form->text()->type($type);
                    break;
            }

            // Create the element attributes and add the element to the form.
            $this->buildAttributes($type, $columnName, $columnObject, $element, $data);
            $form->addElement($columnName, $element);

            // Build labels. They don't have to be used.
            $labelText = $this->getLabelText($columnName);
            $labelElement = $form->label($labelText)->for($columnName);
            $this->setElementClass('label', $labelElement);
            $form->addElement('label_' . $columnName, $labelElement);
        }
        return $form;
    }

    /**
     * Get the column type
     * @param  string $columnName   Name of the column
     * @param  Column $columnObject A column object with col definitions
     * @return string The column type
     */
    protected function getColumnType($columnName, Column $columnObject)
    {
        $type = 'text';

        if (array_key_exists($columnObject->getType()->getName(), $this->map)) {
            $type = $this->map[$columnObject->getType()->getName()];
        }
        if (array_key_exists($columnName, $this->columnOptions)) {
            $info = $this->columnOptions[$columnName];
            if ($info['type']) {
                $type = $info['type'];
            }
        }
        return $type;
    }

    /**
     * Build element attributes
     * @param  string $type Column type
     * @param  string $columnName Column name
     * @param  Column $columnObject Column Object
     * @param  Element $element Form element
     * @param  array|ArrayAccess $data Data to use for populating the values
     * @return void
     */
    protected function buildAttributes($type, $columnName, Column $columnObject, $element, $data)
    {
        $element->id($columnName);
        $element->name($columnName);

        $options = $this->columnOptions[$columnName];

        if ($columnObject->getAutoincrement()) {
            $element->type('hidden');
            $type = 'hidden';
        }

        $value = '';
        if ($columnObject->getDefault()) {
            $value = $columnObject->getDefault();
        }

        // Use isset because array_key_exists does not work with ArrayAccess
        // Radio and Checkbox should not use the data value as the element's value.
        if (isset($data[$columnName]) && ! in_array($type, ['radio', 'checkbox'])) {
            $value = (string) $data[$columnName];
        }

        // Radio and Checkbox should use the data value to determine if it is checked.
        if (isset($data[$columnName]) && in_array($type, ['radio', 'checkbox'])) {
            $checked = (string) $data[$columnName];
            $element->checked($checked);

        }

        $this->setElementValue($element, $value, $options['raw']);
        $this->setElementClass($type, $element);

        if ($columnObject->getLength() && $this->hasMaxlength($element)) {
            $element->maxlength($columnObject->getLength());
        }

        if ($this->isRequired($columnObject, $element)) {
            $element->required();
        }


        $callback = $options['callback'];
        if (is_callable($callback)) {
            $callback($element);
        }
    }

    /**
     * Set the element class based on element type
     * @param string $type Element type
     * @param Element $element The form element
     */
    protected function setElementClass($type, $element)
    {
        $class = null;
        if (array_key_exists($type, $this->defaultClasses)) {
            $class = $this->defaultClasses[$type];
        }

        if ($class) {
            $element->addClass($class);
        }
    }

    /**
     * Set the element's value
     * @param Element $element The form element
     * @param string $value The element value
     * @param boolean $raw Use escaped value or raw.
     */
    protected function setElementValue($element, $value, $raw)
    {

        switch (true) {
            case $element instanceof \Vespula\Form\Element\Select :
                // Select uses "selected". Values are on the options.
                $element->selected($value);
                break;
            case $element instanceof \Vespula\Form\Element\Password :
                $element->value(''); // No support for populating a password form element.
                break;
            default :
                $element->value($value);
                if ($raw) {
                    $element->valueRaw($value);
                }
        }

    }

    /**
     * Determine if the element should have a maxlength
     *
     * @param  Element $element The form element
     * @return boolean True if yes, false if not.
     */
    protected function hasMaxlength($element)
    {
        switch(true) {
            case is_a($element, Checkbox::class) :
            case is_a($element, Radio::class) :
                return false;
                break;
            case is_subclass_of($element, Input::class) :
                return true;
                break;
            default :
                return false;
        }
    }

    /**
     * Determine if the element should be required or not
     *
     * @param  Column $columnObject The column object
     * @param  Element $element The form element
     * @return boolean True for yes, False for no.
     */
    protected function isRequired($columnObject, $element)
    {
        switch(true) {
            case is_a($element, Checkbox::class) :
            case is_a($element, Radio::class) :
                return false;
                break;
            case $columnObject->getNotnull() && ! $columnObject->getAutoincrement() :
                return true;
                break;
            default :
                return false;
        }

    }

    /**
     * Normalize the column options to ensure the keys are set
     * @param  array $cols Columns from the DB
     * @return void
     */
    protected function normalizeOptions($cols)
    {
        $columnNames = array_keys($cols);

        foreach ($this->columnOptions as $colunmName=>$options) {
            $normalized = array_merge($this->defaultOptions, $options);
            $this->columnOptions[$colunmName] = $normalized;
        }
        foreach ($columnNames as $colunmName) {
            if (!array_key_exists($colunmName, $this->columnOptions)) {
                $this->columnOptions[$colunmName] = $this->defaultOptions;
            }
        }
    }

    /**
     * Create basic label text based on the column name
     *
     * @param  string $columnName The column name
     * @return string The label (upper case words based on column name)
     */
    protected function getLabelText($columnName)
    {
        $labelText = str_replace(['-', '_'], ' ', ucwords($columnName, '-_'));
        return $labelText;
    }

    /**
     * Set the map of column types to element types.
     * @param array $map A map of column type to element type
     */
    public function setMap(array $map)
    {
        $this->map = array_merge($this->map, $map);
    }

    /**
     * Set the column options
     * @param array $options The column options (each can have type, raw, callback keys)
     */
    public function setColumnOptions(array $options)
    {
        $this->columnOptions = $options;
    }

    /**
     * Set the classes for each column type
     * @param array $classes Classes keyed by element type
     */
    public function setDefaultClasses(array $classes)
    {
        $this->defaultClasses = $classes;
    }

    /**
     * Set the textarea rows
     * @param integer $cols Number of cols
     */
    public function setTextareaCols($cols)
    {
        $this->textareaCols = $cols;
    }

    /**
     * Set the textarea rows
     * @param integer $rows The number of rows
     */
    public function setTextareaRows($rows)
    {
        $this->textareaRows = $rows;
    }

    /**
     * Set the blacklisted cols
     * @param array $columns Columns to ignore
     */
    public function setBlackList(array $columns)
    {
        $this->blackList = $columns;
    }

    /**
     * Reset the Builder
     */
    public function reset()
    {
        $this->map = [
            'text'=>'textarea',
            'tinytext'=>'textarea',
            'mediumtext'=>'textarea',
            'longtext'=>'textarea',
        ];
        $this->columnOptions = [];
        $this->defaultClasses = [];
        $this->textareaCols = 10;
        $this->textareaRows = 5;
    }
}
