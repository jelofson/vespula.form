<?php

namespace Vespula\Form\Builder;

use Vespula\Form\Form;
use Doctrine\DBAL\Connection;

interface BuilderInterface
{
    public function __construct(Connection $conn);
    public function build(Form $form, $table, $data = null);
    public function setMap(array $map);
    public function setColumnOptions(array $options);
    public function setDefaultClasses(array $classes);
    public function setBlackList(array $columns);
    public function setTextareaCols($cols);
    public function setTextareaRows($rows);
    public function reset();
    
}
