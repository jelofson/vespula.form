<?php
namespace Vespula\Form;

/**
 * This class is used to manage attributes.
 *
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 */
class Attributes
{
    /**
     * The array of attributes and associated values
     * @var array
     */
    protected $attributes = [];

    /**
     * Set an attribute value
     * @param string $attribute The attribute name
     * @param mixed $value The attribute value
     */
    public function set($attribute, $value = null)
    {
        $this->attributes[$attribute] = $value;
    }

    /**
     * Remove an attribute by name
     * @param string $attribute Attribute name
     * @return void
     */
    public function remove($attribute)
    {
        if (array_key_exists($attribute, $this->attributes)) {
            unset($this->attributes[$attribute]);
        }
    }

    /**
     * Check if the attribute store has an attribute by name
     * @param  string $attribute The attribute name
     * @return boolean true on success. false on failure
     */
    public function has($attribute)
    {
        return array_key_exists($attribute, $this->attributes);
    }

    /**
     * Get an attribute by name or false if it doesn't exist.
     * @param  string $attribute
     * @return mixed The attribute value or false
     */
    public function get($attribute)
    {
        if ($this->has($attribute)) {
            return $this->attributes[$attribute];
        }

        return false;
    }

    /**
     * Get all attributes
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    public function reset()
    {
        $this->attributes = [];
    }

    /**
     * Return a string formatted representation of the attributes
     * @return string
     */
    public function __toString()
    {
        if (! $this->attributes) {
            return '';
        }
        $attribs = [];

        foreach ($this->attributes as $name=>$value) {
            if ($value === false) {
                continue;
            }
            if (is_null($value) || $value === true) {
                $attribs[] = $name;
                continue;
            }
            $attribs[] = $name . '="' . $value .'"';
        }

        return ' ' . implode(' ', $attribs);
    }
}
