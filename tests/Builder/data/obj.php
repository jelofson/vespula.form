<?php
namespace Vespula\Form\Builder;

class Obj implements \ArrayAccess {
    private $container = array();

    public function __construct() {
        $this->container = [
            'id'=>2,
            'name'=>'Joe',
            'age'=>26,
            'published'=>1,
            'status'=>'M',
            'created'=>'2017-08-21 16:00:00',
            'profile'=>'Lorem ipsum dolar sit amet.'
        ];
    }

    public function offsetSet($offset, $value): void 
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    public function offsetExists($offset): bool 
    {
        return isset($this->container[$offset]);
    }

    public function offsetUnset($offset): void 
    {
        unset($this->container[$offset]);
    }

    public function offsetGet($offset) 
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }
}
