<?php
namespace Vespula\Form;

use PHPUnit\Framework\TestCase;

class AttributesTest extends TestCase
{
    public function testSetGet()
    {
        $attributes = new Attributes();
        $attributes->set('cat', 'dog');

        $expected = 'dog';
        $actual = $attributes->get('cat');

        $this->assertEquals($expected, $actual);

    }

    public function testSetGetNull()
    {
        $attributes = new Attributes();
        $attributes->set('cat');

        $actual = $attributes->get('cat');

        $this->assertNull($actual);

    }

    public function testGetFalse()
    {
        $attributes = new Attributes();
        $actual = $attributes->get('cat');

        $this->assertFalse($actual);

    }

    public function testRemove()
    {
        $attributes = new Attributes();
        $attributes->set('foo', 'bar');

        $attributes->remove('foo');

        $this->assertFalse($attributes->get('foo'));
    }

    public function testHas()
    {
        $attributes = new Attributes();
        $attributes->set('foo', 'bar');

        $this->assertTrue($attributes->has('foo'));
        $this->assertFalse($attributes->has('bar'));
    }

    public function testGetAttributes()
    {
        $attributes = new Attributes();
        $attributes->set('foo', 'bar');
        $attributes->set('cat', 'dog');
        $attributes->set('horse');

        $expected = [
            'foo'=>'bar',
            'cat'=>'dog',
            'horse'=>null
        ];

        $this->assertEquals($expected, $attributes->getAttributes());
    }

    public function testToString()
    {
        $attributes = new Attributes();
        $attributes->set('foo', 'bar');
        $attributes->set('cat', 'dog');
        $attributes->set('lorem');

        $expected = ' foo="bar" cat="dog" lorem';

        $this->assertEquals($expected, $attributes->__toString());
    }

    public function testReset()
    {
        $attributes = new Attributes();
        $attributes->set('foo', 'bar');
        $attributes->set('cat', 'dog');
        $attributes->set('lorem');
        $attributes->reset();
        $expected = [];
        $this->assertEquals($expected, $attributes->getAttributes());
    }
}
