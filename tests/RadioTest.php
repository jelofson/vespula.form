<?php
namespace Vespula\Form;
use Vespula\Form\Element\Radio;
use PHPUnit\Framework\TestCase;

class RadioTest extends TestCase
{
    public function testCheckedBool()
    {
        $radio = new Radio();
        $radio->checked();
        $expected  = '<input type="radio" checked />';

        $actual = $radio->__toString();
        $this->assertEquals($expected, $actual);

    }

    public function testCheckedVal()
    {
        $radio = new Radio();
        $radio->checked('foo')->value('foo');
        $expected  = '<input type="radio" value="foo" checked />';

        $actual = $radio->__toString();
        $this->assertEquals($expected, $actual);

    }

    public function testLabel()
    {
        $radio = new Radio();
        $radio->label('My Label');
        $expected  = '<input type="radio" /><label>My Label</label>';

        $actual = $radio->__toString();
        $this->assertEquals($expected, $actual);

        $radio->id('foo');

        $expectedFor  = '<input type="radio" id="foo" /><label for="foo">My Label</label>';

        $actualFor = $radio->__toString();
        $this->assertEquals($expectedFor, $actualFor);

    }

    public function testLf()
    {
        $radio = new Radio();
        $radio->lf();
        $expected  = '<input type="radio" />' . PHP_EOL;

        $actual = $radio->__toString();
        $this->assertEquals($expected, $actual);
    }

    public function testUncheckedValue()
    {
        $radio1 = new Radio();
        $radio1->name('radio1')->uncheckedValue('0');

        $radio2 = new Radio();
        $radio2->name('radio2')->lf()->uncheckedValue('false');
        
        $expected1  = '<input name="radio1" value="0" type="hidden"> <input type="radio" name="radio1" />';
        $actual1 = $radio1->__toString();
        
        $expected2  = '<input name="radio2" value="false" type="hidden"> ' . PHP_EOL . '<input type="radio" name="radio2" />';
        $actual2 = $radio2->__toString();
        
        $this->assertStringContainsString($expected1, $actual1);
        $this->assertStringContainsString($expected2, $actual2);
    }
}
