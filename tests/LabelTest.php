<?php
namespace Vespula\Form;
use Vespula\Form\Element\Label;
use Vespula\Form\Element\Text;
use PHPUnit\Framework\TestCase;

class LabelTest extends TestCase
{
    public function testText()
    {
        $label = new Label();
        $label->text('My Label');
        $expected  = '<label>My Label</label>';

        $actual = $label->__toString();
        $this->assertEquals($expected, $actual);
    }

    public function testFor()
    {
        $label = new Label();
        $label->text('My Label')->for('foo');
        $expected  = '<label for="foo">My Label</label>';

        $actual = $label->__toString();
        $this->assertEquals($expected, $actual);
    }

    public function testWrap()
    {
        $text = new Text();
        $label = new Label();
        $label->text('My Label')->wrap($text);

        $expected  = '<label>My Label' . PHP_EOL .
                     '    <input type="text" />' . PHP_EOL .
                     '</label>';

        $actual = $label->__toString();
        $this->assertEquals($expected, $actual);
    }

    public function testWrapAfter()
    {
        $text = new Text();
        $label = new Label();
        $label->text('My Label')->wrapAfter($text);

        $expected  = '<label>'. PHP_EOL .
                     '    <input type="text" />My Label' . PHP_EOL .
                     '</label>';

        $actual = $label->__toString();
        $this->assertEquals($expected, $actual);
    }

    public function testIndent()
    {
        $text = new Text();
        $label = new Label();
        $label->text('My Label')->wrap($text)->indent(2);

        $indent = str_repeat('    ', 2);

        $expected  = '<label>My Label' . PHP_EOL .
                     $indent . '    <input type="text" />' . PHP_EOL .
                     $indent . '</label>';

        $actual = $label->__toString();
        $this->assertEquals($expected, $actual);
    }

    public function testLf()
    {
        $label = new Label();
        $label->text('foo')->lf();
        $expected  = '<label>foo</label>' . PHP_EOL;

        $actual = $label->__toString();
        $this->assertEquals($expected, $actual);

    }

    public function testLfWrap()
    {
        $text = new Text();
        $label = new Label();
        $label->text('foo')->lf()->wrap($text);
        $expected  = '<label>foo' . PHP_EOL .
                     '    <input type="text" />' . PHP_EOL .
                     '</label>' . PHP_EOL;

        $actual = $label->__toString();
        $this->assertEquals($expected, $actual);

    }
}
