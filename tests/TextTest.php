<?php
namespace Vespula\Form;
use Vespula\Form\Element\Text;
use PHPUnit\Framework\TestCase;

class TextTest extends TestCase
{

    public function testTypeText()
    {
        $text = new Text();
        $expected = '<input type="text" />';
        $actual = $text->__toString();

        $this->assertEquals($expected, $actual);
    }

    public function testType()
    {
        $text = new Text();
        $text->type('email');
        $expected = '<input type="email" />';
        $actual = $text->__toString();

        $this->assertEquals($expected, $actual);
    }

    public function testLf()
    {
        $text = new Text();
        $text->lf();
        $expected = '<input type="text" />' . PHP_EOL;
        $actual = $text->__toString();

        $this->assertEquals($expected, $actual);
    }
    
    public function testGetType()
    {
        $text = new Text();
        $text->type('submit');
        $expected = 'submit';
        $actual = $text->getType();

        $this->assertEquals($expected, $actual);
    }
}
