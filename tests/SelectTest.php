<?php
namespace Vespula\Form;
use Vespula\Form\Element\Select;
use PHPUnit\Framework\TestCase;

class SelectTest extends TestCase
{
    public function testBasic()
    {
        $select = new Select();
        $expected  = '<select></select>';

        $actual = $select->__toString();
        $this->assertEquals($expected, $actual);
    }

    public function testBasicOptions()
    {
        $select = new Select();
        $select->options([
            'foo',
            'bar',
        ]);
        $expected  = '<select>' . PHP_EOL .
                     '    <option value="0">foo</option>' . PHP_EOL .
                     '    <option value="1">bar</option>' . PHP_EOL .
                     '</select>';

        $actual = $select->__toString();
        $this->assertEquals($expected, $actual);
    }
    

    public function testOptionValues()
    {
        $select = new Select();
        $select->options([
            'foo'=>'bar',
            'lorem'=>'ipsum',
        ]);
        $expected  = '<select>' . PHP_EOL .
                     '    <option value="foo">bar</option>' . PHP_EOL .
                     '    <option value="lorem">ipsum</option>' . PHP_EOL .
                     '</select>';

        $actual = $select->__toString();
        $this->assertEquals($expected, $actual);
    }

    public function testOptgroup()
    {
        $select = new Select();
        $select->options([
            'foo'=>[
                'a',
                'b'
            ],
            'bar'=>[
                'c'=>'cat',
                'd',
                'x'=>'y'
            ],
        ]);
        $expected  = '<select>' . PHP_EOL .
                     '    <optgroup label="foo">' . PHP_EOL .
                     '        <option value="0">a</option>' . PHP_EOL .
                     '        <option value="1">b</option>' . PHP_EOL .
                     '    </optgroup>' . PHP_EOL .
                     '    <optgroup label="bar">' . PHP_EOL .
                     '        <option value="c">cat</option>' . PHP_EOL .
                     '        <option value="0">d</option>' . PHP_EOL .
                     '        <option value="x">y</option>' . PHP_EOL .
                     '    </optgroup>' . PHP_EOL .
                     '</select>';

        $actual = $select->__toString();
        $this->assertEquals($expected, $actual);
    }

    public function testSelected()
    {
        $select = new Select();
        $select->options([
            'foo'=>'bar',
            'lorem'=>'ipsum',
        ]);
        $select->selected('lorem');
        $expected  = '<select>' . PHP_EOL .
                     '    <option value="foo">bar</option>' . PHP_EOL .
                     '    <option value="lorem" selected>ipsum</option>' . PHP_EOL .
                     '</select>';

        $actual = $select->__toString();
        $this->assertEquals($expected, $actual);
    }

    public function testSelectedOptgroup()
    {
        $select = new Select();
        $select->options([
            'foo'=>[
                'a'=>'a',
                'b'=>'b'
            ],
            'bar'=>[
                'c'=>'cat',
                'd'=>'dog'
            ],
        ]);
        $select->selected('c');
        $expected  = '<select>' . PHP_EOL .
                     '    <optgroup label="foo">' . PHP_EOL .
                     '        <option value="a">a</option>' . PHP_EOL .
                     '        <option value="b">b</option>' . PHP_EOL .
                     '    </optgroup>' . PHP_EOL .
                     '    <optgroup label="bar">' . PHP_EOL .
                     '        <option value="c" selected>cat</option>' . PHP_EOL .
                     '        <option value="d">dog</option>' . PHP_EOL .
                     '    </optgroup>' . PHP_EOL .
                     '</select>';

        $actual = $select->__toString();
        $this->assertEquals($expected, $actual);
    }

    public function testIndent()
    {
        $select = new Select();
        $select->options([
            'foo'=>'bar',
            'lorem'=>'ipsum',
        ]);
        $select->indent(2);

        $indent = str_repeat('    ', 2);
        $expected  = '<select>' . PHP_EOL .
                     $indent .'    <option value="foo">bar</option>' . PHP_EOL .
                     $indent .'    <option value="lorem">ipsum</option>' . PHP_EOL .
                     $indent .'</select>';

        $actual = $select->__toString();
        $this->assertEquals($expected, $actual);
    }

    public function testLf()
    {
        $select = new Select();
        $select->options([
            'foo',
            'bar',
        ])->lf();
        $expected  = '<select>' . PHP_EOL .
                     '    <option value="0">foo</option>' . PHP_EOL .
                     '    <option value="1">bar</option>' . PHP_EOL .
                     '</select>' . PHP_EOL;

        $actual = $select->__toString();
        $this->assertEquals($expected, $actual);
    }
    
    public function testSelectMultiple()
    {
        $select = new Select();
        $select->options([
            'foo',
            'bar',
            'dim'
        ])->attribute('multiple', true)
          ->selected([0,2])
          ->lf();
        $expected  = '<select multiple>' . PHP_EOL .
                     '    <option value="0" selected>foo</option>' . PHP_EOL .
                     '    <option value="1">bar</option>' . PHP_EOL .
                     '    <option value="2" selected>dim</option>' . PHP_EOL .
                     '</select>' . PHP_EOL;

        $actual = $select->__toString();
        $this->assertEquals($expected, $actual);
    }

    public function testSelectMultipleShortcut()
    {
        $select = new Select();
        $select->options([
            'foo',
            'bar',
            'dim'
        ])->multiple()
          ->selected([0,2])
          ->lf();
        $expected  = '<select multiple>' . PHP_EOL .
                     '    <option value="0" selected>foo</option>' . PHP_EOL .
                     '    <option value="1">bar</option>' . PHP_EOL .
                     '    <option value="2" selected>dim</option>' . PHP_EOL .
                     '</select>' . PHP_EOL;

        $actual = $select->__toString();
        $this->assertEquals($expected, $actual);
    }

    public function testDisabledOptions()
    {
        $select = new Select();
        $select->options([
            'foo'=>'bar',
            'lorem'=>'ipsum',
            'lorem2'=>'ipsum2',
        ])->disabledOptions(['foo', 'lorem2']);
        $expected  = '<select>' . PHP_EOL .
                     '    <option value="foo" disabled>bar</option>' . PHP_EOL .
                     '    <option value="lorem">ipsum</option>' . PHP_EOL .
                     '    <option value="lorem2" disabled>ipsum2</option>' . PHP_EOL .
                     '</select>';

        $actual = $select->__toString();
        $this->assertEquals($expected, $actual);
    }

    public function testAllOptionsDisabled()
    {
        $select = new Select();
        $select->options([
            'foo'=>'bar',
            'lorem'=>'ipsum',
            'lorem2'=>'ipsum2',
        ])->allOptionsDisabled(true);
        $expected  = '<select>' . PHP_EOL .
                     '    <option value="foo" disabled>bar</option>' . PHP_EOL .
                     '    <option value="lorem" disabled>ipsum</option>' . PHP_EOL .
                     '    <option value="lorem2" disabled>ipsum2</option>' . PHP_EOL .
                     '</select>';

        $actual = $select->__toString();
        $this->assertEquals($expected, $actual);
        
        $select2 = new Select();
        $select2->options([
            'foo'=>'bar',
            'lorem'=>'ipsum',
            'lorem2'=>'ipsum2',
        ])->allOptionsDisabled(false);
        $expected2  = '<select>' . PHP_EOL .
                     '    <option value="foo">bar</option>' . PHP_EOL .
                     '    <option value="lorem">ipsum</option>' . PHP_EOL .
                     '    <option value="lorem2">ipsum2</option>' . PHP_EOL .
                     '</select>';

        $actual2 = $select2->__toString();
        $this->assertEquals($expected2, $actual2);
    }

    public function testAllOptionsSelected()
    {
        $select = new Select();
        $select->options([
            'foo'=>'bar',
            'lorem'=>'ipsum',
            'lorem2'=>'ipsum2',
        ])->allOptionsSelected(true);
        $expected  = '<select>' . PHP_EOL .
                     '    <option value="foo" selected>bar</option>' . PHP_EOL .
                     '    <option value="lorem" selected>ipsum</option>' . PHP_EOL .
                     '    <option value="lorem2" selected>ipsum2</option>' . PHP_EOL .
                     '</select>';

        $actual = $select->__toString();
        $this->assertEquals($expected, $actual);
        
        $select2 = new Select();
        $select2->options([
            'foo'=>'bar',
            'lorem'=>'ipsum',
            'lorem2'=>'ipsum2',
        ])->allOptionsSelected(false);
        $expected2  = '<select>' . PHP_EOL .
                     '    <option value="foo">bar</option>' . PHP_EOL .
                     '    <option value="lorem">ipsum</option>' . PHP_EOL .
                     '    <option value="lorem2">ipsum2</option>' . PHP_EOL .
                     '</select>';

        $actual2 = $select2->__toString();
        $this->assertEquals($expected2, $actual2);
    }
}
