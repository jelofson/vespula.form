<?php
namespace Vespula\Form;
use Vespula\Form\Element\Checkbox;
use PHPUnit\Framework\TestCase;

class CheckboxTest extends TestCase
{
    public function testTypeCheckbox()
    {
        $cbox = new Checkbox();
        $expected  = '<input type="checkbox" />';

        $actual = $cbox->__toString();
        $this->assertEquals($expected, $actual);

    }
    public function testLf()
    {
        $cbox = new Checkbox();
        $cbox->lf();
        $expected  = '<input type="checkbox" />' . PHP_EOL;

        $actual = $cbox->__toString();
        $this->assertEquals($expected, $actual);

    }

    public function testUncheckedValue()
    {
        $checkbox1 = new Checkbox();
        $checkbox1->name('checkbox1')->uncheckedValue('0');

        $checkbox2 = new Checkbox();
        $checkbox2->name('checkbox2')->lf()->uncheckedValue('false');
        
        $expected1  = '<input name="checkbox1" value="0" type="hidden"> <input type="checkbox" name="checkbox1" />';
        $actual1 = $checkbox1->__toString();
        
        $expected2  = '<input name="checkbox2" value="false" type="hidden"> ' . PHP_EOL . '<input type="checkbox" name="checkbox2" />';
        $actual2 = $checkbox2->__toString();
        
        $this->assertStringContainsString($expected1, $actual1);
        $this->assertStringContainsString($expected2, $actual2);
    }
}
