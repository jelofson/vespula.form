<?php
namespace Vespula\Form;
use Vespula\Form\Element\Hidden;
use PHPUnit\Framework\TestCase;

class HiddenTest extends TestCase
{

    public function testHidden()
    {
        $element = new Hidden();
        $element->value('foo')->id('bar');
        $expected = '<input type="hidden" value="foo" id="bar" />';
        $actual = $element->__toString();

        $this->assertEquals($expected, $actual);
    }

    
}
