<?php
namespace Vespula\Form;

use Vespula\Form\Element\Textarea;
use Vespula\Form\Element\Text;
use Vespula\Form\Element\Radio;
use Vespula\Form\Element\Label;
use Vespula\Form\Element\Hidden;
use PHPUnit\Framework\TestCase;

class ElementTest extends TestCase
{
    public function testAttribute()
    {
        $text = new Textarea();
        $text->attribute('foo', 'bar');

        $expected = '<textarea foo="bar"></textarea>';

        $actual = $text->__toString();

        $this->assertEquals($expected, $actual);

    }

    public function testAttributeNoValue()
    {
        $text = new Textarea();
        $text->attribute('foo');

        $expected = '<textarea foo></textarea>';

        $actual = $text->__toString();

        $this->assertEquals($expected, $actual);

    }

    public function testData()
    {
        $text = new Textarea();
        $text->data('foo', 'bar');

        $expected = '<textarea data-foo="bar"></textarea>';

        $actual = $text->__toString();

        $this->assertEquals($expected, $actual);

    }

    public function testAria()
    {
        $radio = new Radio();
        $radio->aria('checked', 'true');

        $expected = '<input type="radio" aria-checked="true" />';

        $actual = $radio->__toString();

        $this->assertEquals($expected, $actual);

    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testCallBadMethod()
    {
        $this->expectException(\InvalidArgumentException::class);
        $text = new Textarea();
        $text->cow();
    }

    public function testCallValue()
    {
        $text = new Textarea();
        $text->value('foo');

        $expected = '<textarea>foo</textarea>';

        $actual = $text->__toString();

        $this->assertEquals($expected, $actual);

    }
    /**
     * @expectedException InvalidArgumentException
     */
    public function testCallNullInvalid()
    {
        $this->expectException(\InvalidArgumentException::class);
        $text = new Text();
        $text->name();

        $text->__toString();

    }

    public function testCallNull()
    {
        $text = new Text();
        $text->readonly();

        $expected = '<input type="text" readonly />';

        $actual = $text->__toString();
        $this->assertEquals($expected, $actual);

    }


    public function testCallArrayValue()
    {
        $text = new Textarea();
        $text->class([
            'left',
            'button'
        ]);

        $expected = '<textarea class="left button"></textarea>';

        $actual = $text->__toString();

        $this->assertEquals($expected, $actual);

    }

    public function testAddClass()
    {
        $text = new Textarea();
        $text->class('foo');
        $text->addClass('bar');
        $text->addClass('foo');

        $expected = '<textarea class="foo bar"></textarea>';

        $actual = $text->__toString();
        $this->assertEquals($expected, $actual);

    }

    public function testPlaceholder()
    {
        $text = new Text();
        $text->placeholder('text here');

        $expected = '<input type="text" placeholder="text here" />';

        $actual = $text->__toString();
        $this->assertEquals($expected, $actual);
    }

    public function testValue()
    {
        $text = new Text();
        $text->value("<script>alert('foo');</script>");

        $expected = '<input type="text" value="&lt;script&gt;alert(\'foo\');&lt;/script&gt;" />';

        $actual = $text->__toString();
        $this->assertEquals($expected, $actual);
    }

    public function testValueRaw()
    {
        $text = new Text();
        $text->valueRaw("<script>alert('foo');</script>");

        $expected = '<input type="text" value="<script>alert(\'foo\');</script>" />';

        $actual = $text->__toString();
        $this->assertEquals($expected, $actual);
    }

    // Should actually test this with all elements
    public function testAutoLf()
    {
        Text::autoLfOn();
        $text = new Text();

        $expected = '<input type="text" />' . PHP_EOL;

        $actual = $text->__toString();
        Text::autoLfOff();
        $this->assertEquals($expected, $actual);
    }

    public function testIdName()
    {
        $text = new Text();
        $text->idName('foo');

        $expected = '<input type="text" id="foo" name="foo" />';

        $actual = $text->__toString();
        $this->assertEquals($expected, $actual);
    }

    public function testGetAttribute()
    {
        $text = new Text();
        $text->idName('foo');

        $expected = 'foo';

        $actual = $text->getAttribute('id');
        $this->assertEquals($expected, $actual);
    }
    
    public function testIsLabel()
    {
        $text = new Text();
        $label = new Label();
        
        $this->assertTrue($label->isLabel());
        $this->assertFalse($text->isLabel());
    }
    
    public function testIsHidden()
    {
        $text = new Text();
        $hidden = new Hidden();
        
        $this->assertTrue($hidden->isHidden());
        $this->assertFalse($text->isHidden());
    }
}
